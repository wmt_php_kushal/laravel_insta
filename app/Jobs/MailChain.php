<?php

namespace App\Jobs;

use App\Mail\testdata;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class MailChain implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
public $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->user=$user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        dd($this->user);
        $email= new testdata();
        Mail::to('some@email.com')->send($email);
    }
}
