<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Country;
use App\Events\UserCreated;
use App\Jobs\MailChain;
use App\Jobs\SendEmail;
use App\Liker;
use App\Mail\Instagram;
use App\Mail\test;
use App\Mail\testdata;
use App\Post;
use App\Posts_Users;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
//        $c=Comment::all();
//        $pu=Posts_Users::all();
        return view('posts.show_post', ['p' => $post]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create_post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    private function mimeType($path)
    {
        return finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|mimes:jpeg,bmp,png,jpg,mp4,avi,mkv',
            'description' => 'required|max:1000',
        ]);
        $image = $request->file('url');
        $originalname = uniqid('Post_', 15) . '.' . $image->getClientOriginalExtension();
        $path = $image->storeAs('post', $originalname, 'public');
        $typeExtentension = $this->mimetype($request->url);
//        dd($typeExtentension);
        if (substr($typeExtentension, 0, 5) == 'image') {

            $typefile = 'image';
        } elseif (substr($typeExtentension, 0, 5) == 'video') {
            $typefile = 'video';
        }
        $post = Post::create([
            'url' => $path,
            'description' => $request->description,
            'user_id' => Auth::id(),
            'post_type' => $typefile,

            ]);
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::all()->where('id', $id);
        $c = Comment::all();
        return view('posts.single_post', ['p' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        //
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, $id)
//    {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->route('post.index');
    }
    public function postVideo()
    {
        $post = Post::all()->where('post_type', 'video');
//        $comment = Comment::all();
        return view('posts.show_post', ['p' => $post]);
    }

    public function postimage()
    {
        $post = Post::all()->where('post_type', 'image');
//        $c=Comment::all();
//        return view('posts.show_post',['p'=>$post,'c'=>$c]);
        return view('posts.show_post', ['p' => $post]);
    }
    public function likers($id)
    {
        $post = Post::find($id);
        $user = Auth::id();
        $post->users()->attach($user);

//        return redirect()->route('post.index');
        return redirect()->back();
    }

    public function dislikers($id)
    {
        $post = Post::find($id);
        $user = Auth::id();
        $post->users()->detach($user);
//    return redirect()->route('post.index');
        return redirect()->back();
    }
    //country related functions
    public function countryindex()
    {
        $country = Country::all();
        return view('posts.country', ['country' => $country]);
    }
    public function countryshow($id)
    {
        $c = Country::with(['posts'])->find($id);
        return view('posts.show_post', ['p' => $c->posts]);
    }
//    user update and edit
    public function useredit(){
        $user=Auth::user();
        return view('userupdate',['user'=>$user]);
    }
    public function userupdate($id){
        $request=request();
        $image = $request->file('profile_pic');
        $originalname = $request->name . '-' . $request->id.'.jpg';
        $path = $image->storeAs('profile',$originalname,'public');

        $this->validate($request,[
            'name' =>['required'],
            'email' =>['required' ,'email'],
            'country_id' =>['required'],
        ]);
        $user =User::where('id',$id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'profile_pic'=> $path,
            'country_id'=>$request->country_id,
        ]);



    }
//    Comments store
    public function commentstore(Request $request)
    {
//        $c= new Comment();
//        $c->user_id=Auth::id();
//        $c->post_id=$request->post_id;
//        $c->body=$request->cmnt;
//        $c->save();
        $commet=Comment::Create([
            'user_id'=>Auth::id(),
            'post_id'=>$request['post_id'],
            'body'=>$request['cmnt'],
        ]);
        event(new UserCreated($commet));
        return redirect()->route('post.index');

    }
    public function sendmail(){
        $user=User::find(1);

        Mail::to('one@example.com')->cc('one@example.com')->bcc('one@example.com')->send(new Instagram($user));
        view('mails.test');
        return 'Mail has been sent once ,to send it again refresh the page';
//        return redirect()->route('home');
    }
    public function postmail(Request $request){
//        try {
        $user=User::find(1);
////        $user=User::all();
//        dispatch(new SendEmail($user));
////        $when = now()->addSecond(10);
//        Mail::to($request->user())->send(new Instagram($user));
//        view('mails.test',['user'=>$user]);
//        $user=['email'=>'some@email.com'];
//        $emailjob=(new MailChain());
//        dispatch(function () use ($user){
//            $user->publish();
//        });
        $emailjob=(new MailChain());
//        dispatch($emailjob)->delay(Carbon::now()->addSeconds(5));
        dispatch($emailjob);
//
//        SendEmail::withChain([
//            new MailChain
//        ])->dispatch();
//        } catch (\Exception $e) {
//            Bugsnag::notifyException($e);
//
//            throw $e;
//        }
        return 'Mail has been sent once ,to send it again refresh the page';
//        return redirect()->route('home');
    }
//    public function enqueue(Request $request){
//        $user=['email'=>'some@email.com'];
//        $emailjob=(new SendEmail($user))->delay(Carbon::now())->addMinutes(5);
//        SendEmail::dispatch($emailjob)->delay(now()->addMinutes(10));
//    }
}
