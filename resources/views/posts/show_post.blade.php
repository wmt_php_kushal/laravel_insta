@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-mds">
                <div class="card col border-0">
                    <div class="card-header row"></div>
                    <div class="card-header row  ">
                        <a class='btn btn-primary mx-2' name='insert' href="{{route('post.create')}}">{{ __('Add Post') }}</a>
                        <div class="dropdown ">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Options
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class='nav-link' name='country' href="{{route('country.index')}}">Post Country wise </a>
                                <a class='nav-link' name='onlyvideo' href="{{route('post.video')}}">{{ __('Video Posts')}}</a>
                                <a class='nav-link' name='onlyimage' href="{{route('post.image')}}">{{ __('Image Posts')}}</a>
                            </div>
                        </div>
                    </div>
{{--                    <div class="card-header justify-content-around">--}}


                    <div class="card-body justify-content-around border-0">
                    @foreach($p as $post)
{{--                            {{dd($p)}}--}}
                            <div class="card border-0" style="width:600px">
                            <img class="card-img-top" src="{{$post->url}}" alt="Card image" style="width:100%">
                            <div class="card-header"><p class="card-text">{{$post->description}}</p></div>
                            <a href="{{route('post.show',[$post->id])}}" class="btn btn-primary">See Post</a>
                            <div class="card-body ">

                                @if(!(sizeof($post->likeusers)==null))
                                    <a class="btn btn-danger"
                                       href="{{route('dislikers', ['id' => $post->id])}}">Dislike</a>
                                @else
                                    <a class="btn btn-success"
                                       href="{{route('likers', ['id' => $post->id])}}">Like</a>
                                @endif
                                @if(Auth::id()==$post->user_id)
                                    <form action='{{route('post.destroy',[$post->id])}}' method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger mx-2">Delete</button>
                                    </form>
                                @endif
                            </div>

                            <div class="card-header"></div>
                            <div class="card-content d-flex justify-content-center border-0">
                                <form action='{{route('comment.store')}}' class="form-inline" method="post">
                                    @csrf
                                    <input type="text" value="{{$post->id}}" name="post_id" hidden>
                                    <div class="col-sm-12 pb-2 py-2">
                                        <input id="cmnt" type="text" class="form-control @error('cmnt') is-invalid @enderror" name="cmnt" value="{{ old('cmnt') }}" required autocomplete="cmnt" autofocus>
                                        @error('cmnt')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <button type="submit" class="btn btn-success">Comment</button>
                                    </div>
                                </form>
                            </div>
                                <div class="card-content px-3 border-0">
                                    <p><strong>Comments:</strong></p>
                                    <div class="px-5">
                                        {{--                                    {{dd($cc->users->name)}}--}}
                                        <strong>{{$post->user->name}}:</strong>
                                        @foreach($post->comments as $cc)
                                            <span>{{$cc->body}}</span><br>
                                        @endforeach
                                    </div>
                                </div>
                        </div>
                        <br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
