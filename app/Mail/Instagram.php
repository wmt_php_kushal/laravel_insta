<?php

namespace App\Mail;

use App\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Instagram extends Mailable
{
    use Queueable, SerializesModels;

    public $comment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($comment)
//    public function __construct()
    {
        $this->comment=$comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('from@example.com')->view('mails.maildata',['comment'=>$this->comment]);
//        $this->view('mails.test',['user'=>$this->user])->attach('storage/profile/admins-1.jpg');
//        $this->markdown('mails.test');

//        $this->withSwiftMessage(Function($message){
//           $message->getHeaders()->addtextHeader('Custom-Header','Header-value');
//        });
//        return $this->from('example@example.com')->subject('Test Queued Email')->view('mails.test')
//            ->text('mails.test');
    }
}
