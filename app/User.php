<?php

namespace App;

use App\Events\UserCreated;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Foundation\Auth\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class User extends Authenticatable
{
    use Notifiable;

//    protected $dispatchesEvents =['created'=>UserCreated::class];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','country_id','profile_pic',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getProfilePicAttribute($value){
        return url('/storage/'.$value);
    }
    public function posts()
    {
//        return $this->hasMany(Post::class);
        return $this->belongsToMany(Post::class,'posts__users','user_id','post_id');
    }
    public function countries(){
        return $this->belongsTo(Country::class)->with(User::class);
    }
    public function comments(){
        return $this->hasOne(Comment::class);
    }
    public function post(){
                return $this->hasMany(Post::class);

    }
//
}
