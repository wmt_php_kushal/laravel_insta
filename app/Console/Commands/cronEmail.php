<?php

namespace App\Console\Commands;

use App\Mail\Instagram;
use App\Mail\testdata;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class cronEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:Email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $usr=User::find(Auth::id());
//        dd(User::find(Auth::id());
        Mail::to('some@mail.com')->send(new testdata());
        echo 'Output';
    }
}
