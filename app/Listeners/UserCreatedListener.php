<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Jobs\MailChain;
use App\Mail\Instagram;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserCreatedListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
//        dd($comment);
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $usr=User::find(Auth::id());
        Mail::to($usr->email)->send(new Instagram($event->comment));
    }
}
