<?php

namespace App\Jobs;

use App\Mail\Instagram;
use App\Mail\testdata;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->user=$user;
//        $user=$this->user;
    }

    /**
     * Execute the job.
     *
     * @return \Carbon\Carbon
     */
    public function handle()
    {
//        dd($this->user);
        $email= new Instagram();
//        $email= new Instagram();
        Mail::to('some@email.com')->send($email);
    }
}
