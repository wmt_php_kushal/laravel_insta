<?php

namespace App\Console;

use App\Jobs\MailChain;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\DemoCron',
        'App\Console\Commands\cronEmail',

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//         $schedule->command('demo:cron')
//                  ->everyMinute();
//$schedule->command('demo:cron')
//                  ->everyMinute()->sendOutputTo('/home/wmt/Desktop/some.log') ->onOneServer();
          $schedule->command('cron:Email')->everyMinute();
//          $schedule->command('cron:Email')
//                  ->everyMinute()->withoutOverlapping(1);
//         $schedule->command('cron:Email')->everyMinute()->onSuccess(function () {
//            echo 'cronEmailSuccess';
//         });
//         $schedule->command('cron:Email')->everyMinute()->onFailure(function () {
//             echo 'cronEmailFailed';
//         });
//        $schedule->command('cron:Email')->everyMinute()->after(function (){
//            echo "about to start/end";
//        });
//        $schedule->command('cron:Email')->everyMinute()->pingOnSuccess('/Desktop/some.log');
//        $schedule->command('cron:Email')->everyMinute()->when(function () {
//            return true;
//        });
//        $schedule->command('cron:Email')->everyMinute()->skip(function () {
//            return true;
//        });
//        $schedule->command('cron:Email')->everyMinute()->runInBackground();
//        $schedule->command('cron:Email')->everyMinute()->emailOutputTo('foo@example.com');// output ma mail jase aa email id upr
//        $schedule->command('cron:Email')->everyMinute()->emailOutputTo('foo@example.com');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

}
//* * * * * php /Desktop/larvel/laravel_insta schedule:run >> /dev/null 2>&1
