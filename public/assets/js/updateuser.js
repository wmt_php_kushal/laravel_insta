$(document).ready(function() {
    $('#userupdate').validate({
        rules:{
            name: {
                required : true
            },
            email:{
                required: true,
                email: true
            },
            country_id: {
                required : true
            }
        }
    })

});
