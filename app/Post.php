<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Testing\MimeType;
use Illuminate\Support\Facades\Auth;
use const http\Client\Curl\AUTH_ANY;

class Post extends Model
{
    protected $fillable=[
        'post_type','url','description','user_id',
    ];
    public function getUrlAttribute($value){
        return url('/storage/'.$value);
}

public function comments(){
        return $this->hasMany(Comment::class);
}

    public function country(){
        return $this->hasOneThrough(Country::class,User::class);
}
    public function users(){
        return $this->belongsToMany(User::class,'posts__users','post_id','user_id');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function likeusers(){
        return $this->users()->where('user_id',Auth::id());
    }
}
