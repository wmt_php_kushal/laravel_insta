<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\Instagram;
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::Resource('/post','PostController');
Route::post('/comment','PostController@commentstore')->name('comment.store');
//Route::put('/post/status/{id}','PostController@status')->middleware(['web','auth'])->name('post.status');
Route::put('/post/status/{id}','PostController@status')->name('post.status');
Route::view('/countries/','posts.country')->name('post.country');
Route::get('/post/country/','PostController@showCountry')->name('post.countryid');
Route::get('/video/post','PostController@postVideo')->name('post.video');
Route::get('/image/post','PostController@postImage')->name('post.image');
Route::get('/country/post','PostController@countryindex')->name('country.index');
Route::get('/country/post/{id}','PostController@countryshow')->name('country.show');

//Route::get('/country/{country?}', 'CountryController@myCountryList')->name('country.list');
//Route::Resource('/country','CountryController');
Route::get('/like','PostController@likeordislike');
Route::get('/liked/{id}','PostController@likers')->name('likers');
Route::get('/disliked/{id}','PostController@dislikers')->name('dislikers');

//Route::Resource('/like','LikerController');
Route::get('/useredit','PostController@useredit')->name('user.edit');
Route::put('/userupdadte/{id}','PostController@userupdate')->name('user.update');
//
Route::get('/send-mail','PostController@postmail')->name('send-mail');
