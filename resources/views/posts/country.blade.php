@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Country wise Posts') }}</div>
                    <div class="card-body">
                        <div class="form-group d-flex justify-content-centre">
                            <div class="form-group row mx-2 mb-0">
                                <h3>Select the Country</h3>
                                <div class="dropdown justify-content-center">
                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        Country
                                    </button>
                                    <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
                                        @foreach($country as $c)
                                            <a class="dropdown-item" href="{{route('country.show',['id'=>$c->id])}}">{{$c->name}}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


