@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-mds">
                <div class="card">
                    <div class="card-header"><a class='btn btn-primary' name='insert' href="{{route('post.create')}}">{{ __('Add Post') }}</a></div>
                    <div class="card-header justify-content-around"><a class='btn btn-primary' name='onlyvideo' href="{{route('post.video')}}">{{ __('Video Posts')}}</a>
                        <a class='btn btn-primary' name='onlyimage' href="{{route('post.image')}}">{{ __('Image Posts')}}</a></div>
                    <div class="card-body justify-content-around">
                        @foreach($p as $p)
                            {{--                            {{dd($p)}}--}}
                            <div class="card" style="width:400px">
                                <img class="card-img-top" src="{{$p->url}}" alt="Card image" style="width:100%">
                                <div class="card-header"><p class="card-text">{{$p->description}}</p></div>
                                <a href="{{route('post.index')}}" class="btn btn-primary">See all Posts</a>
                                <div class="card-body d-flex justify-content-center">

                                    @if($p->status==1)
                                        <form action='/post/status/{{$p->id}}' method="post">
                                            @csrf
                                            @method('put')
                                            <small class="text-primary">You Liked</small>
                                            <button type="submit" class="btn btn-success">Dislike</button>
                                        </form>
                                    @endif
                                    @if($p->status==0)
                                        <form action='/post/status/{{$p->id}}' method="post">
                                            @csrf
                                            @method('put')
                                            <button type="submit" class="btn btn-danger">Like</button>
                                        </form>
                                    @endif
                                    @if(Auth::id()==$p->user_id)

                                        <form action='/post/{{$p->id}}' method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    @endif


                                </div>
                                <div class="card-content px-3" >
                                    <p><strong>Comments:</strong></p>
                                    <div class="px-5">
                                        {{--                                    {{$p->id}}--}}
                                        {{--                                    {{\App\Country::all()->where('post_id',$p->id)}}--}}
                                        {{--                                    {{dd($p->id)}}--}}
                                        @foreach($c->where('post_id',$p->id) as $c)
                                            {{--                                        {{dd($c->where('post_id',$p->id))}}--}}
                                            {{--                                        @if($p->id==$c->post_id)--}}
                                            <p>{{$c->body}}</p>
                                            {{--                                        @endif--}}
                                        @endforeach
                                    </div>
                                </div>
                                <div class="card-header"></div>
                                <div class="card-content d-flex justify-content-center ">
                                    <form action='{{route('comment.store')}}' class="form-inline" method="post">
                                        @csrf
                                        <input type="text" value="{{$p->id}}" name="post_id" hidden>
                                        <div class="col-sm-12 pb-2 py-2">
                                            <input id="cmnt" type="text" class="form-control @error('cmnt') is-invalid @enderror" name="cmnt" value="{{ old('cmnt') }}" required autocomplete="cmnt" autofocus>
                                            @error('cmnt')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                            <button type="submit" class="btn btn-success">Comment</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
