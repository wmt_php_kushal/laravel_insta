@extends('layouts.app')
@section('scripts')
    <script src="{{ asset('/assets/js/createpost.js') }}" defer></script>
@endsection
{{--@extends('messages')--}}
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Add Post') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data" id="createpost">
                            @csrf
                            <div class="form-group row">
                                <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('url') }}</label>
                                <div class="col-md-6">
                                    <input id="url" type="file" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ old('url') }}"  autocomplete="url" autofocus>
                                    @error('url')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Caption') }}</label>
                                <div class="col-md-6">
{{--                                    <textarea id="description" class="form-control" @error('description') is-invalid @enderror name="description" autocomplete="description" autofocus rows="3"></textarea>--}}
                                    <textarea type='text' id="description" class="form-control" @error('description') is-invalid @enderror name="description" >{{ old('description') }}</textarea>
                                    @error('description')
                                    <div class="alert alert-danger " role="alert invalid-feedback ">
                                            <strong>{{ $message }}</strong>
                                        </div>
{{--                                    {{dd($message)}}--}}
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <button class="btn btn-success" type="submit">Add Post</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


